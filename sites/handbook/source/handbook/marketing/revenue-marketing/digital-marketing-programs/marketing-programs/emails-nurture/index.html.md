---
layout: markdown_page
title: "Emails & Nurture Programs"
description: "Overview of emails and nurture programs at GitLab - this page is being moved under Demand Generation."
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

**NOTE: this page has been moved under the Campaigns handbook page (the anchors here are being preserved until 2021-02-15 to avoid 404 conusion, and allow visitors to bookmark the new location).** The contents at time of movement have stayed the same. Please bookmark the new location.

Please help us! If you arrived at this page from somewhere else in the handbook, please consdier contributing to this update by correcting the link on the previous page with the correct link below.

Thank you! Your help is very much appreciated!

# Overview
See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#overview](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#overview)

### Holiday coverage for severity::1 security vulnerabilities email communication

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#holiday-coverage](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#holiday-coverage)

## Email marketing calendar

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#calendar](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#calendar)

## Email Marketing Best Practices

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#best-practices](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#best-practices)

## Email Templates

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#email-templates](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#email-templates)

## Ad-hoc (one-time) emails - requesting an email

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#email-requests](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#email-requests)

#### Important note regarding audience segmentation efforts and efficiency
See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#noate-audience-segmentation](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#noate-audience-segmentation)

#### Email Request Issue Template
See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#issue-templates](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#issue-templates)

#### Need-to-know details for the email request

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#email-request-details](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#email-request-details)

#### Types of email requests

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#email-request-types](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#email-request-types)

#### Approvals and notifications for email requests

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#email-request-approval](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#email-request-approval)

#### Email review protocol

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#review-protocol](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#review-protocol)

## Email Nurture Programs

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#nurture-programs](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#nurture-programs)

### Visualization of current nurture streams

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#active-nurtures-visualization](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#active-nurtures-visualization)

### Requesting to add leads to a nurture program

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#add-to-nurture-request](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#add-to-nurture-request)

#### Interim additions to nurture programs (while future state and system requirements being built out)

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#add-to-nurture-request](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#add-to-nurture-request)

### Current Nurture Programs

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#active-nurture-programs](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#active-nurture-programs)

#### SaaS trial nurture

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#nurture-trial-saas](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#nurture-trial-saas)

#### Self-managed trial nurture

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#nurture-trial-self-hosted](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#nurture-trial-self-hosted)

#### Integrated campaign nurtures

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#nurtures-gtm-motions](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#nurtures-gtm-motions)

### Newsletter

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter)

#### Process for bi-weekly newsletter

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter-process](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter-process)

#### Creating the newsletter in Marketo

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter-marketo-creating](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter-marketo-creating)

#### Editing the newsletter in Marketo

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter-marketo-editing](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter-marketo-editing)

#### Sending newsletter test/samples from Marketo

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter-marketo-testing](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter-marketo-testing)

#### Sending the newsletter

See (and bookmark) new handbook location here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter-marketo-sending](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#newsletter-marketo-sending)